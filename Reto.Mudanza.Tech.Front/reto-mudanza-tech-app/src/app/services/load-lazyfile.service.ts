import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoadLazyfileService {
  url= "http://localhost:53344";
  constructor(private httpClient: HttpClient) { }

  GuardarArchivo(file: File, document:string) {

    const body = new FormData();
    body.append("file", file);
    body.append("documentUser", document);

    const headers = new HttpHeaders(
      {
          'Content-Type': 'multipart/form-data'
      })

    return this.httpClient.post(`${this.url}/api/lazyLoading`, body, { observe: 'response'});

  }
  Descargar(){
    return this.httpClient.get(`${this.url}/api/lazyLoading/Download`, { responseType: 'blob' });
  }
}
