﻿using Microsoft.EntityFrameworkCore;
using Reto.Mudanza.Tech.Models;

namespace Reto.Mudanza.Tech.Business
{
    public class ApplicationContext: DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<User> UserTraces { get; set; }
    }
}