﻿using System;
using System.Collections.Generic;
using System.IO;
using Reto.Mudanza.Tech.Business.Interfaces;
using Reto.Mudanza.Tech.Models;
using Reto.Mudanza.Tech.Utilities.Utils;

namespace Reto.Mudanza.Tech.Business
{
    public class FileManager : IFileManager
    {

        private readonly IGeneralConfiguration _generalConfiguration;

        public FileManager(IGeneralConfiguration generalConfiguration)
        {
            _generalConfiguration = generalConfiguration;
        }

        /// <summary>
        /// Method that reads the file
        /// </summary>
        /// <param name="fileInformation">Object containing the input file information</param>
        /// <returns>List of values ​​to process</returns>
        public List<int> ReadFile(FileInformation fileInformation)
        {
            try
            {
                var readFile = new List<int>();
                SaveFile(fileInformation);

                if (!File.Exists(fileInformation.TargetPath)) throw new Exception($"{_generalConfiguration.FileNotExists}");
                var file = new StreamReader(fileInformation.TargetPath);
                string line;
                while ((line = file.ReadLine()) != null)
                    readFile.Add(Convert.ToInt32(line));

                return readFile;

            }
            catch (IOException e)
            {
                Log.Log.Logger($"ReadFile --> FileManager --> Error processing file { e.Message}");
                return null;
            }
        }

        /// <summary>
        /// Method that stores a file
        /// </summary>
        /// <param name="file">Object containing the input file information</param>
        public void SaveFile(FileInformation file)
        {

            try
            {
                if (!Directory.Exists(file.PathFileName))
                {
                    Directory.CreateDirectory(file.PathFileName);
                    DeleteFile(file.TargetPath);
                }
                Path.Combine(file.PathFileName, file.FileName);   
                using (var stream = new FileStream(file.TargetPath, FileMode.Create))
                    file.File.CopyTo(stream);
                    
            }
            catch (IOException e)
            {
                Log.Log.Logger($"SaveFile --> FileManager --> Error saving file { e.Message}");
            }
        }

        /// <summary>
        /// Method that writes the content to the output file
        /// </summary>
        /// <param name="fileContent">Object containing the input file information</param>
        public void WriteFile(FileInformation fileContent)
        {
            try
            {
                if (!File.Exists(fileContent.TargetOutputPath))
                {
                    File.Create(fileContent.TargetOutputPath).Dispose();
                    using (TextWriter textWriter = new StreamWriter(fileContent.TargetOutputPath))
                        textWriter.WriteLine(fileContent.FileContent);
                }
                else
                {
                    DeleteFile(fileContent.TargetOutputPath);
                    File.Create(fileContent.TargetOutputPath).Dispose();
                    using (TextWriter textWriter = new StreamWriter(fileContent.TargetOutputPath))
                        textWriter.WriteLine(fileContent.FileContent);
                }
            }
            catch (IOException e)
            {
                Log.Log.Logger($"WriteFile --> FileManager --> Error writing file { e.Message}");
            }
        }

        /// <summary>
        /// Method that deletes a file
        /// </summary>
        /// <param name="path">File path</param>
        public void DeleteFile(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }
            catch (IOException e)
            {
                Log.Log.Logger($"DeleteFile --> FileManager --> Error delete file { e.Message}");
            }

        }
    }
}
