﻿using Reto.Mudanza.Tech.Business.Interfaces;
using Reto.Mudanza.Tech.Models;

namespace Reto.Mudanza.Tech.Business
{
    public class UserTrace : IUserTrace
    {
        private readonly ApplicationContext _applicationContext;

        public UserTrace(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public User Create(User entity)
        {
            _applicationContext.Set<User>().Add(entity);
            return entity;
        }
    }
}