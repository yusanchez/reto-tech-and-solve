﻿using System.Collections.Generic;
using Reto.Mudanza.Tech.Models;

namespace Reto.Mudanza.Tech.Business.Interfaces
{
    public interface IFileManager
    {
        void SaveFile(FileInformation file);
        void WriteFile(FileInformation fileContent);
        void DeleteFile(string path);
        List<int> ReadFile(FileInformation fileName);
    }
}