﻿using Reto.Mudanza.Tech.Models;

namespace Reto.Mudanza.Tech.Business.Interfaces
{
    public interface IUserTrace
    {
        User Create(User entity);
    }
}