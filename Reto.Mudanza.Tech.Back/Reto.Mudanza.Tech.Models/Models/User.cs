﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Reto.Mudanza.Tech.Models
{
    [Table("USERTRACE")]
    public class User
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity), Key()]
        [Column("ID", Order = 0)]
        public int RegisterId { get; set; }
        [Column("DOCUMENT", Order = 1)]
        public string Document { get; set; }
        [Column("REGISTERDATE", Order = 2)]
        public DateTime RegisterDate{ get; set; }
        
    }
}