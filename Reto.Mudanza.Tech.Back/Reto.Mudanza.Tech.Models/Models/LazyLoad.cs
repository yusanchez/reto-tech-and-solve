﻿using System.Collections.Generic;

namespace Reto.Mudanza.Tech.Models
{
    public class LazyLoad
    {
        public int AmountOfTrips { get; set; }
        public List<int> AmountOfWeight { get; set; }
    }
}