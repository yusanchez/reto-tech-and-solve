﻿using System;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Reto.Mudanza.Tech.Models;
using Reto.Mudanza.Tech.Utilities.Utils;
using Reto.Mudanza.Tech.Business.Interfaces;

namespace Reto.Mudanza.Tech.Api.Controllers
{

    /// <summary>
    /// Lazy loading controller
    /// </summary>
    [Route("api/lazyLoading")]
    [ApiController]
    public class LazyLoadingController : ControllerBase
    {
        private readonly IFileManager _fileManager;
        private readonly IProcessFileManager _processFileManager;
        private readonly IGeneralConfiguration _generalConfiguration;
        private readonly IUserTrace _userTrace;


        /// <summary>
        /// Controller LazyLoading
        /// </summary>
        /// <param name="fileManager"></param>
        /// <param name="processFileManager"></param>
        /// <param name="generalConfiguration"></param>
        /// <param name="userTrace"></param>
        public LazyLoadingController(IFileManager fileManager, 
            IProcessFileManager processFileManager, IGeneralConfiguration generalConfiguration, IUserTrace userTrace)
        {
            _fileManager = fileManager;
            _processFileManager = processFileManager;
            _generalConfiguration = generalConfiguration;
            _userTrace = userTrace;
        }

        /// <summary>
        /// Method to process file
        /// </summary>
        /// <param name="file">File name</param>
        /// <param name="documentUser">User document</param>
        /// <returns>Status</returns>
        [HttpPost, DisableRequestSizeLimit]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Post(IFormFile file,string documentUser)
        {
            try
            {
                var user = new User()
                {
                    Document = documentUser,
                    RegisterDate = DateTime.Now
                };
                _userTrace.Create(user);
                var fileInformation = new FileInformation();
                if (file == null || file.Length == 0) return NotFound(new { status = false, text = "File content is empty" });

                fileInformation.FileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                fileInformation.UploadFolderName = _generalConfiguration.UploadFolder;
                fileInformation.PathFileName = fileInformation.UploadFolderName;
                fileInformation.TargetPath = Path.Combine(fileInformation.PathFileName, fileInformation.FileName);
                fileInformation.File = file;

                var listOfValues = _fileManager.ReadFile(fileInformation);
                fileInformation.FileContent = _processFileManager.ProcessFile(listOfValues);
                fileInformation.TargetOutputPath = Path.Combine(fileInformation.UploadFolderName, _generalConfiguration.FileNameOutput);

                _fileManager.WriteFile(fileInformation);

                return Ok(new { status = true, text = "Process success" });
            }
            catch (Exception e)
            {
                Log.Log.Logger($"Error processing file = {e.Message}");
                return BadRequest($"Error while processing file {e.Message}");
            }
        }
        /// <summary>
        /// Download file
        /// </summary>
        /// <returns>file downloaded</returns>
        [HttpGet]
        [Route("Download")]
        public FileStream Download()
        {
            try
            {
                var router = $"{_generalConfiguration.UploadFolder}{_generalConfiguration.FileNameOutput}";
                return new FileStream(router, FileMode.Open, FileAccess.Read);
            }
            catch (Exception e)
            {
                Log.Log.Logger($"Error download file = {e.Message}");
                return null;
            }
           
        }

    }
}