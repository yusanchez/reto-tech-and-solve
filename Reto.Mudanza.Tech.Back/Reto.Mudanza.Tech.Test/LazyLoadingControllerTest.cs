using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Reto.Mudanza.Tech.Api.Controllers;
using Reto.Mudanza.Tech.Business.Interfaces;
using Reto.Mudanza.Tech.Models;
using Reto.Mudanza.Tech.Utilities.Utils;
using Xunit;

namespace Reto.Mudanza.Tech.Test
{
    public class LazyLoadingControllerTest
    {
        [Fact]
        public void VerifyPostNotFound()
        {
            var moqFileManager = new Mock<IFileManager>(); 
            var moqProcessFile = new Mock<IProcessFileManager>(); 
            var moqGeneralConfig = new Mock<IGeneralConfiguration>(); 
            var moqUserMock = new Mock<IUserTrace>();
            // Arrange.
            var file = new Mock<IFormFile>();
           
            var controller = new LazyLoadingController(moqFileManager.Object, moqProcessFile.Object,
                moqGeneralConfig.Object, moqUserMock.Object);
            var inputFile = file.Object;

            // Act.
            var result =  controller.Post(inputFile,"121313");

            //Assert.
            file.Verify();
            Assert.IsType<NotFoundObjectResult> (result);
        }

        [Fact]
        public void VerifyPost()
        {
            var moqFileManager = new Mock<IFileManager>();
            var moqProcessFile = new Mock<IProcessFileManager>();
            var moqGeneralConfig = new Mock<IGeneralConfiguration>();
            var moqUserMock = new Mock<IUserTrace>();
            var user = new User()
            {
                Document = "1234",
                RegisterDate = DateTime.Now
            };
            var listOfValue = new List<int>()
            {
                1,
                2,
                3,
                4
            };

            // Arrange.
            var file = new Mock<IFormFile>();
            var sourceImg = File.OpenRead(@"D:\Personal\lazy_loading_example_inputUpdate.txt");
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(sourceImg);
            writer.Flush();
            ms.Position = 0;
            const string fileName = "lazy_loading_example_inputUpdate.txt";
            file.Setup(f => f.FileName).Returns(fileName).Verifiable();
            file.Setup(_ => _.CopyToAsync(It.IsAny<Stream>(), It.IsAny<CancellationToken>()))
                .Returns((Stream stream, CancellationToken token) => ms.CopyToAsync(stream, token))
                .Verifiable();

            var controller = new LazyLoadingController(moqFileManager.Object, moqProcessFile.Object,
                moqGeneralConfig.Object, moqUserMock.Object);
            var inputFile = file.Object;

            // Act.
            var result = controller.Post(inputFile, "121313");

            //Assert.
            file.Verify();
        }


    }
}
